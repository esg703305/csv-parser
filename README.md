# csv-parserApplication

Simple CSV parse which takes an absolute path to customer csv file, parses each line and then post
the customer data to customer-service endpoint.

Run following commands

`mvn clean package`

`java -jar target/csv-parser-application-1.0-SNAPSHOT-jar-with-dependencies.jar`

![img.png](img.png)

![img_1.png](img_1.png)

# Approach Taken

The console asks for absolute path directory and passes all the CSV file in that directory.