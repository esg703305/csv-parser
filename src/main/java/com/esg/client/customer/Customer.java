package com.esg.client.customer;

public record Customer(String customerReference,
                       String customerName,
                       String addressLine1,
                       String addressLine2,
                       String town,
                       String county,
                       String country,
                       String postcode) {}