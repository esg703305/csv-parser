package com.esg.client.customer;

import com.esg.configuration.ApplicationConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;

public class CustomerServiceClient {
    private final String url;
    private final ObjectMapper mapper;
    private final HttpClient client;

    public CustomerServiceClient(ApplicationConfiguration configuration) {
        this.client = HttpClient.newHttpClient();
        this.url = configuration.getCustomerServiceUrl();
        this.mapper = new ObjectMapper();
    }

    public String postCustomerData(Customer body) throws URISyntaxException, IOException, InterruptedException {
        byte[] sampleData = mapper.writeValueAsString(body).getBytes();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(url))
                .headers("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers
                        .ofInputStream(() -> new ByteArrayInputStream(sampleData)))
                .build();
       var response =  client.send(request, HttpResponse.BodyHandlers.ofString());
       return response.body();
    }
}
