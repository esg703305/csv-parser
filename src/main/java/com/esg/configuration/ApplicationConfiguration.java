package com.esg.configuration;

import java.util.Properties;

public class ApplicationConfiguration {
    private final Properties properties;

    public ApplicationConfiguration() throws Exception {
        properties = new Properties();
        properties.load(ApplicationConfiguration.class.getClassLoader().getResourceAsStream("application.properties"));
    }

    public String getCustomerServiceUrl() {
        return properties.getProperty("customer-service-url");
    }
}
