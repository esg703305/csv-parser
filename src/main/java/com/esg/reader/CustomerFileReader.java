package com.esg.reader;

import com.esg.client.customer.Customer;
import com.esg.client.customer.CustomerServiceClient;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class CustomerFileReader {

  private static final String TOTAL_SUCCESS = "%d out of %d records were successfully saved in customer service";
  private final CustomerServiceClient client;

  public CustomerFileReader(CustomerServiceClient client) {
    this.client = client;
  }

  public void readCsvFiles(String path) throws IOException, CustomerInvalidFieldException, CsvException {
    File dir = new File(path);
    File[] files = dir.listFiles((dir1, name) -> name.toLowerCase().endsWith(".csv"));

    if (files != null) {
      for (var file : files) {
        readCsvFile(file.getAbsolutePath());
      }
    }
  }
  public void readCsvFile(String fileName)
      throws IOException, CsvException, CustomerInvalidFieldException {
    int totalSuccess = 0;
    try (CSVReader reader = new CSVReader(new FileReader(fileName))) {
      List<String[]> lines = reader.readAll();
      int lineNumber = 1;
      List<String> messagesFromClientService = new ArrayList<>();
      messagesFromClientService.add("| line_number | message from server |");
      for (String[] line : lines) {
        if (line.length != 8) {
          throw new CustomerInvalidFieldException();
        }
        Customer customer = new Customer(
            line[0],
            line[1],
            line[2],
            line[3],
            line[4],
            line[5],
            line[6],
            line[7]);
        var success = client.postCustomerData(customer);
        if ("ok".equals(success)) {
          totalSuccess++;
        }
        if (success != null) {
          messagesFromClientService.add(String.format("| %d | %s |", lineNumber, success));
        }

        lineNumber++;
      }
      if (messagesFromClientService.size() > 1) {
        System.out.printf((TOTAL_SUCCESS) + "%n", totalSuccess, lines.size());
        messagesFromClientService.forEach(System.out::println);
      }
    } catch (URISyntaxException | InterruptedException e) {
      throw new RuntimeException(e);
    }
  }
}
