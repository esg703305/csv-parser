package com.esg.reader;

public class CustomerInvalidFieldException extends Exception {

  public CustomerInvalidFieldException() {
    super("CSV file does not have necessary fields");
  }

}
