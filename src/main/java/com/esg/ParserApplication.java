package com.esg;

import com.esg.client.customer.CustomerServiceClient;
import com.esg.configuration.ApplicationConfiguration;
import com.esg.reader.CustomerFileReader;
import java.util.Scanner;

public class ParserApplication {

  private final Scanner scanner;
  private final CustomerFileReader reader;

  public ParserApplication() throws Exception {
    var configuration = new ApplicationConfiguration();
    reader = new CustomerFileReader(new CustomerServiceClient(configuration));
    this.scanner = new Scanner(System.in);
  }
  public static void main(String[] args) throws Exception {
    ParserApplication parserApplication = new ParserApplication();
    parserApplication.parseFile();
  }
  private void parseFile() {
    boolean parseCsv = true;
    while (parseCsv) {
      var path = getFilePath();
      try {
        reader.readCsvFiles(path);
        parseCsv = continueParsing();
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    scanner.close();
  }
  private String getFilePath() {
    System.out.println("Please enter the file path for parsing customer csv file");
    return scanner.next();
  }
  private boolean continueParsing() {
    System.out.println("Enter X to quit the application or C to continue");
    String output = scanner.next();
    return !"X".equalsIgnoreCase(output);
  }

}