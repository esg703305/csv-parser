package com.esg.reader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import com.esg.client.customer.Customer;
import com.esg.client.customer.CustomerServiceClient;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
class CustomerFileReaderTest {

  private CustomerServiceClient client;

  @Test
  void testSuccessFulParsing()
      throws Exception {

    client = mock(CustomerServiceClient.class);
    Customer customer = new Customer("client_reference_001",
        "Toby John",
        "09 Downing Street",
        " Well",
        " Norfolk",
        " Essex",
        " United Kingdom",
        " RH98 7JB"
    );
    Customer notValidCustomer = new Customer("",
        "Toby John",
        "09 Downing Street",
        " Well",
        " Norfolk",
        " Essex",
        " United Kingdom",
        " RH98 7JB"
    );

    when(client.postCustomerData(eq(customer))).thenReturn("ok");
    when(client.postCustomerData(eq(notValidCustomer))).thenReturn("some error");

    String path = "src/main/resources/customers.csv";

    CustomerFileReader reader = new CustomerFileReader(client);

    reader.readCsvFile(path);

  }

  @Test
  void testWrongLNumberOfFields()
      throws Exception {
    client = mock(CustomerServiceClient.class);
    String path = "src/main/resources/invalid_length_customer.csv";
    CustomerFileReader reader = new CustomerFileReader(client);
    try {
      reader.readCsvFile(path);
      fail();
    } catch (CustomerInvalidFieldException e) {
      verifyNoInteractions(client);
      assertEquals("CSV file does not have necessary fields", e.getMessage());
    }
  }
}